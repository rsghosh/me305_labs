'''@file                    main.py
   @brief                   This file contains the main function for Lab 0x03.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 2, 2021
'''

import encoder
import task_encoder
import task_user
import task_motor
import drv8847
import pyb

S0_INIT         = 0
S1_ZERO1        = 1
S2_ZERO2        = 2
S3_PRINT1       = 3
S4_PRINT2       = 4
S5_DELTA1       = 5
S6_DELTA2       = 6
S7_MOTOR1       = 7
S8_MOTOR2       = 8
S9_CLEAR_FAULT  = 9
S10_COLLECT1    = 10
S11_COLLECT2    = 11
S12_END         = 12
S13_USER_INPUT  = 13

if __name__ == '__main__':
    # encoder setup

    ## Pin A of encoder1
    pinA1 = pyb.Pin(pyb.Pin.board.PB6, pyb.Pin.IN)

    ## Pin B of encoder1
    pinB1= pyb.Pin(pyb.Pin.board.PB7, pyb.Pin.IN)

    ## Pin A of encoder2
    pinA2 = pyb.Pin(pyb.Pin.board.PC6, pyb.Pin.IN)

    ## Pin B of encoder2
    pinB2 = pyb.Pin(pyb.Pin.board.PC7, pyb.Pin.IN)

    ## Timer for encoder1
    tim4 = pyb.Timer(4, prescaler=0, period=65535)

    ## Timer channel1 for encoder1
    t4ch1 = tim4.channel(1, pyb.Timer.ENC_A, pin=pinA1)

    ## Timer channel2 for encoder1
    t4ch2 = tim4.channel(2, pyb.Timer.ENC_B, pin=pinB1)

    ## Timer for encoder2
    tim8 = pyb.Timer(8, prescaler=0, period=65535)

    ## timer channel3 for encoder2
    t48h1 = tim8.channel(1, pyb.Timer.ENC_A, pin=pinA2)

    ## timer channel4 for encoder2
    t8ch2 = tim8.channel(2, pyb.Timer.ENC_B, pin=pinB2)

    ## Encoder1
    enc1 = encoder.Encoder(pinA1, pinB1, tim4)

    ## Encoder2
    enc2 = encoder.Encoder(pinA2, pinB2, tim8)


    # motor setup

    ## nSleep pin for motor driver
    nSleep = pyb.Pin(pyb.Pin.board.PA15, pyb.Pin.OUT_PP)

    ## nFault pin for motor driver
    nFault = pyb.Pin(pyb.Pin.board.PB2, pyb.Pin.IN)

    ## Input1 for motor driver
    in1 = pyb.Pin(pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
    
    ## Input2 for motor driver
    in2 = pyb.Pin(pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
    
    ## Input3 for motor driver
    in3 = pyb.Pin(pyb.Pin.board.PB0, pyb.Pin.OUT_PP)
    
    ## Input4 for motor driver
    in4 = pyb.Pin(pyb.Pin.board.PB1, pyb.Pin.OUT_PP)

    ## Timer for motor driver
    tim3 = pyb.Timer(3, freq=20000)

    ## Timer channel1 for motor driver
    t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=in1)
    
    ## Timer channel2 for motor driver
    t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=in2)
    
    ## Timer channel3 for motor driver
    t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin=in3)
    
    ## Timer channel4 for motor driver
    t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin=in4)

    # Create a motor driver object and two motor objects
    
    ## Motor driver
    motor_drv = drv8847.DRV8847(nSleep, nFault)
    
    ## Motor 1
    motor_1 = motor_drv.motor(in1, in2, t3ch1, t3ch2)
    
    ## Motor 2
    motor_2 = motor_drv.motor(in3, in4, t3ch3, t3ch4)


    ## Shared variables for the encoder and user tasks
    shared_vars = {
        'state':        S0_INIT,
        'ref time':     0,
        'duty cycle':   None,
        'result':       0,
        'result2':      0,
        'unread result':  False
    }

    ## Task Encoder
    t_enc = task_encoder.Task_Encoder(enc1, enc2, 1, shared_vars)

    ## Task User Interface
    t_user = task_user.Task_User(2, shared_vars)

    ## Task Motor
    t_motor = task_motor.Task_Motor(motor_drv, motor_1, motor_2, 1, shared_vars)

    # loop tasks
    while(True):
        t_enc.run()
        t_motor.run()
        t_user.run()
