'''@file                    task_encoder.py
   @brief                   This file contains the encoder task for Lab 0x03.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 2, 2021
'''

import encoder
import utime
import math

S0_INIT         = 0
S1_ZERO1        = 1
S2_ZERO2        = 2
S3_PRINT1       = 3
S4_PRINT2       = 4
S5_DELTA1       = 5
S6_DELTA2       = 6
S7_MOTOR1       = 7
S8_MOTOR2       = 8
S9_CLEAR_FAULT  = 9
S10_COLLECT1    = 10
S11_COLLECT2    = 11
S12_END         = 12

class Task_Encoder:
    '''@brief               An encoder task object
       @details             A class for getting data from an encoder driver.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                November 2, 2021
    '''

    def __init__(self, enc1, enc2, period, shared_vars):
        '''@brief               Constructor for encoder task.
           @details             Sets class variables for the encoder task object.
           @param enc1          The 1st encoder object to control.
           @param enc2          The 2nd encoder object to control.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## The 1st encoder object to control
        self.enc1 = enc1

        ## The 2nd encoder object to control
        self.enc2 = enc2

        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the encoder and user tasks
        self.shared_vars = shared_vars
        
        self.update_next_time()

    def run(self):
        '''@brief           Runs the encoder task.
           @details         Controls the encoder driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            # update
            self.enc1.update()
            self.enc2.update()
            
            # collect requested value
            if self.shared_vars['unread result'] == False:
                if self.shared_vars['state'] == S1_ZERO1:
                    self.enc1.set_position(0)
                elif self.shared_vars['state'] == S2_ZERO2:
                    self.enc2.set_position(0)   
                elif self.shared_vars['state'] == S3_PRINT1:
                    self.shared_vars['result'] = self.enc1.get_position()
                elif self.shared_vars['state'] == S4_PRINT2:
                    self.shared_vars['result'] = self.enc2.get_position()
                elif self.shared_vars['state'] == S5_DELTA1:
                    self.shared_vars['result'] = self.enc1.get_delta()
                elif self.shared_vars['state'] == S6_DELTA2:
                    self.shared_vars['result'] = self.enc2.get_delta()
                elif self.shared_vars['state'] == S10_COLLECT1:
                    self.shared_vars['result'] = self.enc1.get_position()
                    self.shared_vars['result2'] = self.enc1.get_delta()
                elif self.shared_vars['state'] == S11_COLLECT2:
                    self.shared_vars['result'] = self.enc2.get_position()
                    self.shared_vars['result2'] = self.enc2.get_delta()
                else:
                    return
                self.shared_vars['unread result'] = True

    def update_next_time(self):
        '''@brief           Returns the next time the encoder task should run.
           @details         Adds period to the current time to calculate the next time the encoder task should run.
           @return          The next time the encoder task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
