'''@file                    task_motor.py
   @brief                   This file contains the motor task for Lab 0x03.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 2, 2021
'''

import DRV8847
import utime

S0_INIT         = 0
S1_ZERO1        = 1
S2_ZERO2        = 2
S3_PRINT1       = 3
S4_PRINT2       = 4
S5_DELTA1       = 5
S6_DELTA2       = 6
S7_MOTOR1       = 7
S8_MOTOR2       = 8
S9_CLEAR_FAULT  = 9
S10_COLLECT1    = 10
S11_COLLECT2    = 11
S12_END         = 12
S13_USER_INPUT  = 13

class Task_Motor:
    '''@brief               A motor task object
       @details             A class for controlling 2 motors.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                November 2, 2021
    '''

    def __init__(self, motor_driver, motor1, motor2, period, shared_vars):
        '''@brief               Constructor for motor task.
           @details             Sets class variables for the motor task.
           @param motor_driver  DRV8847 object.
           @param motor1        1st Motor object.
           @param motor2        2nd Motor object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Motor driver
        self.motor_driver = motor_driver
        
        ## Motor 1
        self.motor1 = motor1
        
        ## Motor 2
        self.motor2 = motor2
        
        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the tasks
        self.shared_vars = shared_vars

        self.motor_driver.enable()
        self.update_next_time()
    
    def run(self):
        '''@brief           Runs the motor task.
           @details         Controls the motor driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            if self.shared_vars['unread result'] == False:
                if self.shared_vars['state'] == S7_MOTOR1:
                    # set motor 1 duty cyclce
                    self.motor1.set_duty(int(self.shared_vars['duty cycle']))
                elif self.shared_vars['state'] == S8_MOTOR2:
                    # set motor 2 duty cycle
                    self.motor2.set_duty(int(self.shared_vars['duty cycle']))
                elif self.shared_vars['state'] == S9_CLEAR_FAULT:
                    self.motor_driver.enable()
                else:
                    return
                self.shared_vars['unread result'] = True

    def update_next_time(self):
        '''@brief           Returns the next time the motor task should run.
           @details         Adds period to the current time to calculate the next time the motor task should run.
           @return          The next time the motor task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
