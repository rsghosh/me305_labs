'''@file                    task_user.py
   @brief                   This file contains the user task for Lab 0x03.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 2, 2021
'''

import pyb
import utime
import math

S0_INIT         = 0
S1_ZERO1        = 1
S2_ZERO2        = 2
S3_PRINT1       = 3
S4_PRINT2       = 4
S5_DELTA1       = 5
S6_DELTA2       = 6
S7_MOTOR1       = 7
S8_MOTOR2       = 8
S9_CLEAR_FAULT  = 9
S10_COLLECT1    = 10
S11_COLLECT2    = 11
S12_END         = 12
S13_USER_INPUT  = 13

class Task_User:
    '''@brief               A user task object
       @details             A class for taking in user input and responding to it.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                November 2, 2021
    '''

    def __init__(self, period, shared_vars):
        '''@brief               Constructor for user task.
           @details             Sets class variables for the user task object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Serial port
        self.ser_port = pyb.USB_VCP()
        
        ## Current state
        self.state = S0_INIT
        
        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the tasks
        self.shared_vars = shared_vars

        self.update_next_time()
        self.reset_state()

        ## Whether data collection is in progress
        self.started_collecting = False

        ## List for storing timestamps for collected data
        self.time_lst = []

        ## List for storing positions for collected data
        self.pos_lst = []

        ## List for storing delta values for collected data
        self.delta_lst = []

        ## Time increment in milliseconds for collecting data
        self.t_increment = 100

        ## Time to collect next sample
        self.next_sample = 0

        ## String for collecting user input digits for motor duty cycle
        self.input = ''

        ## Next state to go to after collecting user input
        self.next_state = 0


    def reset_state(self):
        '''@brief           Resets the state of the user task.
           @details         Changes state to 0 and prints options for the user.
        '''

        self.shared_vars['state'] = S0_INIT
        print('''Enter a commmand:\n
            z - Zero the position of encoder 1\n
            Z - Zero the position of encoder 2\n
            p - Print out the position of encoder 1\n
            P - Print out the position of encoder 2\n
            d - Print out the delta for encoder 1\n
            D - Print out the delta for encoder 2\n
            m - Set the duty cycle for motor 1\n
            M - Set the duty cycle for motor 2\n
            c or C - Clear a fault condition triggered by the DRV8847\n
            g - Collect encoder 1 data for 30 seconds
            and print it to PuTTY as a comma separated list\n
            G - Collect encoder 2 data for 30 seconds
            and print it to PuTTY as a comma separated list\n
            s or S - End data collection prematurely\n''')

    def run(self):
        '''@brief           Runs the user task.
           @details         Requests or collects data from an encoder task and updates the state based on user input.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()
            
            # continue current state
            if self.shared_vars['unread result']:
                if (self.shared_vars['state'] == S1_ZERO1
                    or self.shared_vars['state'] == S2_ZERO2):
                    self.reset_state()
                elif (self.shared_vars['state'] == S3_PRINT1
                    or self.shared_vars['state'] == S4_PRINT2
                    or self.shared_vars['state'] == S5_DELTA1
                    or self.shared_vars['state'] == S6_DELTA2):
                    print(self.shared_vars['result'])
                    self.reset_state()
                elif (self.shared_vars['state'] == S7_MOTOR1
                    or self.shared_vars['state'] == S8_MOTOR2):
                    self.reset_state()
                elif (self.shared_vars['state'] == S9_CLEAR_FAULT):
                    self.reset_state()
                elif (self.shared_vars['state'] == S10_COLLECT1
                    or self.shared_vars['state'] == S11_COLLECT2):
                    # get current time
                    now = utime.ticks_ms()
                    # if collection has not started already, set the reference time
                    # and the next time to collect data
                    if not self.started_collecting:
                        self.shared_vars['ref time'] = now
                        self.next_sample = now
                        self.started_collecting = True
                    # collect data if time passed is less than 30 seconds
                    # and t_increment has passed since the last data collection
                    if utime.ticks_diff(now, self.shared_vars['ref time']) <= 30000:
                        if utime.ticks_diff(now, self.next_sample) >= 0:
                            # set next_time
                            self.next_sample = utime.ticks_add(now, self.t_increment)
                            # add current time relative to ref
                            self.time_lst.append(utime.ticks_diff(now, self.shared_vars['ref time']))
                            # add current encoder position
                            self.pos_lst.append(self.shared_vars['result'])
                            # add currrent encoder delta
                            self.delta_lst.append(self.shared_vars['result2'])
                    # if 30 seconds has passed, print the readings
                    else:
                        self.print_readings(self.time_lst, self.pos_lst, self.delta_lst)
                        # clear the readings and reset state
                        self.time_lst = []
                        self.pos_lst = []
                        self.delta_lst = []
                        self.started_collecting = False
                        self.reset_state()
                elif self.shared_vars['state'] == S12_END:
                    # print the readings
                    self.print_readings(self.time_lst, self.pos_lst, self.delta_lst)
                    # clear the readings and reset state
                    self.time_lst = []
                    self.pos_lst = []
                    self.delta_lst = []
                    self.started_collecting = False
                    self.reset_state()
                self.shared_vars['unread result'] = False

            # update state
            if self.ser_port.any():
                user_in = self.ser_port.read(1)

                if user_in == b'z':
                    self.shared_vars['state'] = S1_ZERO1
                elif user_in == b'Z':
                    self.shared_vars['state'] = S2_ZERO2
                elif user_in == b'p':
                    self.shared_vars['state'] = S3_PRINT1
                elif user_in == b'P':
                    self.shared_vars['state'] = S4_PRINT2
                elif user_in == b'd':
                    self.shared_vars['state'] = S5_DELTA1
                elif user_in == b'D':
                    self.shared_vars['state'] = S6_DELTA2
                elif user_in == b'm':
                    self.shared_vars['state'] = S13_USER_INPUT
                    self.next_state = S7_MOTOR1
                elif user_in == b'M':
                    self.shared_vars['state'] = S13_USER_INPUT
                    self.next_state = S8_MOTOR2
                elif (user_in == b'c' or user_in == b'C'):
                    self.shared_vars['state'] = S9_CLEAR_FAULT 
                elif user_in == b'g':
                    self.shared_vars['state'] = S10_COLLECT1
                elif user_in == b'G':
                    self.shared_vars['state'] = S11_COLLECT2
                elif (user_in == b's' or user_in == b'S'):
                    self.shared_vars['state'] = S12_END
                    self.shared_vars['unread result'] = True
                elif self.shared_vars['state'] == S13_USER_INPUT:
                    # collect numbers for user input
                    if user_in == b'0' or user_in == b'1' or user_in == b'2' or user_in == b'3' or user_in == b'4' or user_in == b'5' or user_in == b'6' or user_in == b'7' or user_in == b'8' or user_in == b'9': 
                        self.input += user_in.decode("utf-8")
                    # minus sign
                    elif user_in == b'-' and len(self.input) == 0:
                        self.input += '-'
                    # backspace key
                    elif user_in == b'\b' and len(self.input) > 0:
                        self.input = self.input[:len(self.input) - 1]
                    # return key (end user input)
                    elif user_in == b'\r' or user_in == b'\n':
                        self.shared_vars['state'] = self.next_state
                        self.shared_vars['duty cycle'] = int(self.input)
                        self.input = ''


    def update_next_time(self):
        '''@brief           Returns the next time the user task should run.
           @details         Adds period to the current time to calculate the next time the user task should run.
           @return          The next time the user task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

    def print_readings(self, time_lst, pos_lst, delta_lst):
        '''@brief           Prints a list of encoder readings with timestamps.
           @details         Prints encoder readings in "<timestamp>, <position>" format, with one reading per line.
        '''

        print('time, pos, delta:')
        for i in range(len(time_lst)):
            print('{}, {}, {}'.format(time_lst[i]/1000, pos_lst[i]*2*math.pi/4000, delta_lst[i]*2*math.pi/4000))
