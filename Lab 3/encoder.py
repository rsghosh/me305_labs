'''@file                    encoder.py
   @brief                   This file contains the encoder driver for Lab 0x03.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    September 19, 2021
'''

class Encoder:
    '''@brief               An encoder driver object
       @details             A class for driving an encoder.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                September 19, 2021
    '''

    def __init__(self, pinA, pinB, timer):
        '''@brief           Constructor for encoder driver.
           @details         Sets class variables for the encoder driver object.
           @param pinA      First pin of the encoder.
           @param pinB      Second pin of the encoder.
           @param timer     Timer used for the encoder.
        '''

        ## First pin of the encoder
        self.pinA = pinA

        ## Second pin of the encoder
        self.pinB = pinB

        ## Timer used for the encoder
        self.timer = timer

        ## Last encoder timer count
        self.last_count = 0

        ## Last encoder position
        self.last_pos = 0

        ## Current encoder position
        self.curr_pos = 0

    def update(self):
        '''@brief           Read the encoder position and update variables.
           @details         Reads encoder position and updates curr_pos, last_pos_, and last_count.
        '''

        self.last_pos = self.curr_pos
        self.curr_pos += self.timer.counter() - (self.last_pos % 65535)
        # overflow
        if self.last_pos - self.curr_pos > 32767:
            self.curr_pos += 65535
        # underflow
        elif self.curr_pos - self.last_pos > 32767:
            self.curr_pos -= 65535
        # current position = last position + delta in counts
        #self.curr_pos = self.last_pos + new_count - self.last_count
        #self.last_count = new_count

    def get_position(self):
        '''@brief           Returns the current position of the encoder.
           @details         Returns the last recorded position of the encoder, stored in curr_pos.
           @return          The curret position of the encoder.
        '''

        return self.curr_pos

    def set_position(self, position):
        '''@brief           Sets the position of the encoder.
           @details         Sets the encoder position to a specific number passed into the function.
           @param position  Position to set the encoder to.
        '''

        self.last_pos += position - self.curr_pos
        self.curr_pos = position
        self.timer.counter(position)
        self.last_count = self.timer.counter()

    def get_delta(self):
        '''@brief           Returns the encoder's delta.
           @details         Returns the difference between the last 2 recorded positions of the encoder.
           @return          The delta between the last 2 recorded positions of the encoder.
        '''

        return self.curr_pos - self.last_pos
