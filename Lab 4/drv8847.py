'''@file                    drv8847.py
   @brief                   This file contains the motor driver and motor classes for Lab 0x04.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 16, 2021
'''

import pyb
import utime

class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to ceate one or more objects of the
                    Motor class which can be used to perform motor control.

                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''

    def __init__(self, nSleep, nFault):
        ''' @brief          Constructor for DRV8847.'
            @details        Sets class variables for the DRV8847 object.
            @param nSleep   nSleep pin.
            @param nFault   nFault pin.
        '''

        ## nSleep pin
        self.nSleep = nSleep
        
        ## nFault pin
        self.nFault = nFault

        ## whether a fault has been triggered
        self.fault_triggered = False

        self.set_up_interrupt()

    def enable(self):
        ''' @brief          Brings the DRV8847 out of sleep mode.
            @details        Disables the fault interrupt, enables the motor driver, and re-enables
                            the fault interrupt. 
        '''

        self.FaultInt.disable()
        self.nSleep.high()
        utime.sleep_us(25)
        self.FaultInt.enable()
        self.fault_triggered = False

    def disable(self):
        ''' @brief          Puts the DRV8847 in sleep mode.
            @details        Sets nSleep to low to disable the motor driver.
        '''
        
        self.nSleep.low()

    def fault_cb(self, IRQ_src):
        ''' @brief          Callback function to run on fault condition.
            @param IRC_src  The source of the interrupt request.
            @details        Disables the motor driver when a fault is detected.
        '''

        self.disable()
        self.fault_triggered = True

    def set_up_interrupt(self):
        ''' @brief          Creates the fault interrupt.
            @details        Sets up a fault interrupt for the motor driver to call fault_cb().
        '''
        ## Fault interrupt
        self.FaultInt = pyb.ExtInt(self.nFault, mode=pyb.ExtInt.IRQ_FALLING,
                        pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)

    def motor(self, in1, in2, tch1, tch2):
        return Motor(in1, in2, tch1, tch2)

class Motor:
    ''' @brief          A motor class for one channel of the DRV8847.
        @details        Objects of this class can be used to apply PWM to a given
                        DC motor.
    '''

    def __init__(self, in1, in2, tch1, tch2):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
        '''

        ## 1st input pin
        self.in1 = in1

        ## 2nd input pin
        self.in2 = in2

        ## 1st timer channel
        self.tch1 = tch1

        ## 2nd timer channel
        self.tch2 = tch2

    def set_duty(self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWM signal sent to the motor
        '''

        if duty > 0:
            self.tch1.pulse_width_percent(duty)
            self.tch2.pulse_width_percent(0)
        else:
            self.tch2.pulse_width_percent(-duty)
            self.tch1.pulse_width_percent(0)

if __name__ == '__main__':

    ## nSleep pin
    nSleep = pyb.Pin(pyb.Pin.board.PA15, pyb.Pin.OUT_PP)
    
    ## nFault pin
    nFault = pyb.Pin(pyb.Pin.board.PB2, pyb.Pin.IN)
    
    ## Input1
    in1 = pyb.Pin(pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
    
    ## Input2
    in2 = pyb.Pin(pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
    
    ## Input3
    in3 = pyb.Pin(pyb.Pin.board.PB0, pyb.Pin.OUT_PP)
    
    ## Input4
    in4 = pyb.Pin(pyb.Pin.board.PB1, pyb.Pin.OUT_PP)

    ## Timer
    tim3 = pyb.Timer(3, freq=20000)

    ## Timer channel1
    t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=in1)
    
    ## Timer channel2
    t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=in2)
    
    ## Timer channel3
    t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin=in3)
    
    ## Timer channel4
    t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin=in4)

    # Create a motor driver object and two motor objects

    ## Motor driver
    motor_drv = DRV8847(nSleep, nFault)
    
    ## Motor1
    motor_1 = motor_drv.motor(in1, in2, t3ch1, t3ch2)
    
    ## Motor2
    motor_2 = motor_drv.motor(in3, in4, t3ch3, t3ch4)

    # Enable the motor driver
    motor_drv.enable()

    # Set the duty cycle of the first motor to 10 percent and the duty cycle of
    # the second motor to 20 percent
    motor_1.set_duty(30)
    motor_2.set_duty(60)

    utime.sleep(5)
    motor_1.set_duty(0)
    motor_2.set_duty(0)
