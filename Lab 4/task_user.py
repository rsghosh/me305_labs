'''@file                    task_user.py
   @brief                   This file contains the user task for Lab 0x04.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 16, 2021
'''

import pyb
import utime
import math

S0_INIT         = 0
S1_COLLECT1     = 1
S2_COLLECT2     = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4
S5_USER_INPUT   = 5

class Task_User:
    '''@brief               A user task object
       @details             A class for taking in user input and responding to it.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                November 16, 2021
    '''

    def __init__(self, period, shared_vars):
        '''@brief               Constructor for user task.
           @details             Sets class variables for the user task object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Serial port
        self.ser_port = pyb.USB_VCP()
        
        ## Current state
        self.state = S0_INIT
        
        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the tasks
        self.shared_vars = shared_vars

        self.update_next_time()
        self.reset_state()

        ## Whether data collection is in progress
        self.started_collecting = False

        ## List for storing timestamps for collected data
        self.time_lst = []

        ## List for storing speeds for collected data
        self.speed_lst = []

        ## List for storing actuation levels for collected data
        self.actuation_lst = []

        ## Time increment in milliseconds for collecting data
        self.t_increment = 100

        ## Time to collect next sample
        self.next_sample = 0

        ## String for collecting user input digits for motor duty cycle
        self.input = ''

        ## Next state to go to after collecting user input
        self.next_state = 0


    def reset_state(self):
        '''@brief           Resets the state of the user task.
           @details         Changes state to 0 and prints options for the user.
        '''

        self.shared_vars['state'] = S0_INIT
        print('''Enter a commmand:\n
            1 - Perform a step response on motor 1
            2 - Perform a step response on motor 2
            c or C - Clear a fault condition triggered by the DRV8847\n
            s or S - End data collection prematurely\n''')

    def run(self):
        '''@brief           Runs the user task.
           @details         Sets motor speeds and collects data from encoders based on user input.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()
            
            # continue current state
            if not (self.shared_vars['request clear fault'] 
                    or self.shared_vars['request motor1 stop']
                    or self.shared_vars['request motor2 stop']):
                if (self.shared_vars['state'] == S3_CLEAR_FAULT):
                    self.reset_state()
                elif (self.shared_vars['state'] == S1_COLLECT1
                    or self.shared_vars['state'] == S2_COLLECT2):
                    # transition to end state if there is a fault
                    if self.shared_vars['fault detected']:
                        print("Motor fault occurred.")
                        self.shared_vars['state'] = S4_END
                    # else collect data
                    else:
                        # get current time
                        now = utime.ticks_ms()
                        # if collection has not started already, set the reference time
                        # and the next time to collect data
                        if not self.started_collecting:
                            self.shared_vars['ref time'] = now
                            self.next_sample = now
                            self.started_collecting = True
                        # collect data if time passed is less than 10 seconds
                        # and t_increment has passed since the last data collection
                        if utime.ticks_diff(now, self.shared_vars['ref time']) <= 10000:
                            if utime.ticks_diff(now, self.next_sample) >= 0:
                                # set next_time
                                self.next_sample = utime.ticks_add(now, self.t_increment)
                                # add current time relative to ref
                                self.time_lst.append(utime.ticks_diff(now, self.shared_vars['ref time']))
                                # add current speed
                                self.speed_lst.append(self.shared_vars['measured vel'])
                                # add currrent actuation level
                                self.actuation_lst.append(self.shared_vars['pwm'])
                        # if 10 seconds has passed, stop the motor and transition to end state
                        else:
                            # stop the motor
                            if self.shared_vars['state'] == S1_COLLECT1:
                                self.shared_vars['request motor1 stop'] = True
                            else:
                                self.shared_vars['request motor2 stop'] = True
                            # transition to end state
                            self.shared_vars['state'] = S4_END
                elif self.shared_vars['state'] == S4_END:
                    # print the readings
                    self.print_readings(self.time_lst, self.speed_lst, self.actuation_lst)
                    # clear the readings and reset state
                    self.time_lst = []
                    self.speed_lst = []
                    self.actuation_lst = []
                    self.started_collecting = False
                    self.reset_state()
                elif self.shared_vars['state'] == S5_USER_INPUT:
                    # get Kp
                    Kp = input("Enter P gain: ")
                    # get velocity setpoint
                    setpoint = input("Enter a velocity setpoint: ")

                    self.shared_vars['Kp'] = float(Kp)
                    self.shared_vars['setpoint'] = float(setpoint)
                    self.shared_vars['state'] = self.next_state

            # update state
            if self.ser_port.any():
                user_in = self.ser_port.read(1)

                if user_in == b'1':
                    self.shared_vars['state'] = S5_USER_INPUT
                    self.next_state = S1_COLLECT1
                elif user_in == b'2':
                    self.shared_vars['state'] = S5_USER_INPUT
                    self.next_state = S2_COLLECT2
                elif (user_in == b'c' or user_in == b'C'):
                    self.shared_vars['state'] = S3_CLEAR_FAULT 
                elif (user_in == b's' or user_in == b'S'):
                    # stop the motor
                    if self.shared_vars['state'] == S1_COLLECT1:
                        self.shared_vars['request motor1 stop'] = True
                    elif self.shared_vars['state'] == S2_COLLECT2:
                        self.shared_vars['request motor2 stop'] = True
                    # transition to end state
                    self.shared_vars['state'] = S4_END


    def update_next_time(self):
        '''@brief           Returns the next time the user task should run.
           @details         Adds period to the current time to calculate the next time the user task should run.
           @return          The next time the user task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

    def print_readings(self, time_lst, speed_lst, actuation_lst):
        '''@brief           Prints a list of encoder readings with timestamps.
           @details         Prints encoder readings in "<timestamp>, <position>" format, with one reading per line.
        '''

        print('time (s), speed (rad/s), actuation level (%):')
        for i in range(len(time_lst)):
            print('{}, {}, {}'.format(time_lst[i]/1000, speed_lst[i], actuation_lst[i]))
