'''@file                    closedloop.py
   @brief                   This file contains the ClosedLoop class for Lab 0x04.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 16, 2021
'''

class ClosedLoop:
    '''@brief               A ClosedLoop object
       @details             A P controller class for generating an actuation value based on measured and reference values and a gain. 
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                November 16, 2021
    '''

    def __init__(self, Kp, max_pwm):
        '''@brief               Constructor for closedloop controller.
           @details             Sets class variables for the closedloop object.
           @param Kp            Proportional gain.
           @param max_pwm       Maximum actuation value.
        '''

        ## Proportional gain
        self.Kp = Kp

        ## Maximum actuation value
        self.max_pwm = max_pwm

    def update(self, measured, reference):
        '''@brief           Gives an actuation value.
           @details         Returns an actuation value baed on measured value, reference value, proportional gain, and max value.
           @param measured  Measured value.
           @param reference Target value.
           @return          Actuation value.
        '''

        pwm = int(self.Kp * (reference - measured))
        if pwm < 0:
            return max(pwm, -self.max_pwm)
        return min(pwm, self.max_pwm)

    def get_Kp(self):
        '''@brief       Returns Kp
           @details     Returns Kp, the proportional gain.
           @return      Kp, the proportional gain.
        '''

        return self.Kp

    def set_Kp(self, Kp):
        '''@brief       Sets Kp.
           @details     Sets Kp (the proportional gain) to whatever is passed in.
           @param Kp    Value to set the proportional gain to.
        '''
        self.Kp = Kp
