'''@file                    task_encoder.py
   @brief                   This file contains the encoder task for Lab 0x04.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 16, 2021
'''

import encoder
import utime
import math

S0_INIT         = 0
S1_COLLECT1     = 1
S2_COLLECT2     = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4
S5_USER_INPUT   = 5

class Task_Encoder:
    '''@brief               An encoder task object
       @details             A class for getting data from an encoder driver.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                November 16, 2021
    '''

    def __init__(self, enc1, enc2, period, shared_vars):
        '''@brief               Constructor for encoder task.
           @details             Sets class variables for the encoder task object.
           @param enc1          The 1st encoder object to control.
           @param enc2          The 2nd encoder object to control.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## The 1st encoder object to control
        self.enc1 = enc1

        ## The 2nd encoder object to control
        self.enc2 = enc2

        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the encoder and user tasks
        self.shared_vars = shared_vars
        
        self.update_next_time()

    def run(self):
        '''@brief           Runs the encoder task.
           @details         Controls the encoder driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            # update
            self.enc1.update()
            self.enc2.update()
            
            # collect requested value
            if self.shared_vars['state'] == S1_COLLECT1:
                # set result to velocity in rad/s
                self.shared_vars['measured vel'] = self.enc1.get_delta() * (2*math.pi/4000) / (self.period/1000)
            elif self.shared_vars['state'] == S2_COLLECT2:
                # set result to velocity in rad/s
                self.shared_vars['measured vel'] = self.enc2.get_delta() * (2*math.pi/4000) / (self.period/1000)

    def update_next_time(self):
        '''@brief           Returns the next time the encoder task should run.
           @details         Adds period to the current time to calculate the next time the encoder task should run.
           @return          The next time the encoder task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
