'''@file                    task_motor.py
   @brief                   This file contains the motor task for Lab 0x04.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    November 16, 2021
'''

import DRV8847
import closedloop
import utime

S0_INIT         = 0
S1_COLLECT1     = 1
S2_COLLECT2     = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4
S5_USER_INPUT   = 5

class Task_Motor:
    '''@brief               A motor task object
       @details             A class for controlling 2 motors.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                November 16, 2021
    '''

    def __init__(self, motor_driver, motor1, motor2, period, shared_vars):
        '''@brief               Constructor for motor task.
           @details             Sets class variables for the motor task.
           @param motor_driver  DRV8847 object.
           @param motor1        1st Motor object.
           @param motor2        2nd Motor object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Motor driver
        self.motor_driver = motor_driver
        
        ## Motor 1
        self.motor1 = motor1
        
        ## Motor 2
        self.motor2 = motor2
        
        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the tasks
        self.shared_vars = shared_vars

        ## Closed loop controller for motor1
        self.cl1 = closedloop.ClosedLoop(10, 100)

        ## Closed loop controller for motor2
        self.cl2 = closedloop.ClosedLoop(10, 100)

        self.motor_driver.enable()
        self.update_next_time()
    
    def run(self):
        '''@brief           Runs the motor task.
           @details         Controls the motor driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            if self.shared_vars['state'] == S1_COLLECT1:
                # check for fault
                if self.motor_driver.fault_triggered:
                    self.shared_vars['fault detected'] = True
                    self.motor1.set_duty(0)
                # else set motor 1 duty cycle
                else:
                    # check if Kp needs to be changed
                    if not self.shared_vars['Kp'] is None:
                        self.cl1.set_Kp(self.shared_vars['Kp'])
                        self.shared_vars['Kp'] = None
                    # set duty cycle
                    pwm = self.cl1.update(self.shared_vars['measured vel'], self.shared_vars['setpoint'])
                    self.motor1.set_duty(pwm)
                    self.shared_vars['pwm'] = pwm
            elif self.shared_vars['state'] == S2_COLLECT2:
                # check for fault
                if self.motor_driver.fault_triggered:
                    self.shared_vars['fault detected'] = True
                    self.motor2.set_duty(0)
                # else set motor 2 duty cycle
                else:
                    # check if Kp needs to be changed
                    if not self.shared_vars['Kp'] is None:
                        self.cl2.set_Kp(self.shared_vars['Kp'])
                        self.shared_vars['Kp'] = None
                    # set duty cycle
                    pwm = self.cl2.update(self.shared_vars['measured vel'], self.shared_vars['setpoint'])
                    self.motor2.set_duty(pwm)
                    self.shared_vars['pwm'] = pwm
            elif self.shared_vars['state'] == S4_END:
                # stop motor
                if self.shared_vars['request motor1 stop']:
                    self.motor1.set_duty(0)
                    self.shared_vars['request motor1 stop'] = False
                if self.shared_vars['request motor2 stop']:
                    self.motor2.set_duty(0)
                    self.shared_vars['request motor2 stop'] = False
            elif self.shared_vars['state'] == S3_CLEAR_FAULT:
                self.motor_driver.enable()
                self.shared_vars['request clear fault'] = False
                self.shared_vars['fault detected'] = False

    def update_next_time(self):
        '''@brief           Returns the next time the motor task should run.
           @details         Adds period to the current time to calculate the next time the motor task should run.
           @return          The next time the motor task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
