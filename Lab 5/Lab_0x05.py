'''@file                    Lab_0x05.py
   @brief                   This file contains the code for Lab 0x05.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 10, 2021
'''

from pyb import I2C
import utime
import struct

# https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf

# calibration procedure - see page 47

# modes (see page 20):
CONFIGMODE      = 0
ACCONLY         = 1
MAGONLY         = 2
GYROONLY        = 3
ACCMAG          = 4
ACCGYRO         = 5
MAGGYRO         = 6
ACCMAG          = 7
IMU             = 8
COMPASS         = 9
M4G             = 10
NDOF_FMC_OFF    = 11
NDOF            = 12

# registers (see page 50):
OPR_MODE            = 0x3D
CALIB_STAT          = 0x35
ACC_OFFSET_X_LSB    = 0x55  # calibration constants include 55-6A
EUL_HEADING_LSB     = 0x1A  # euler angles include 1A-1F
#   0x1A - heading LSB
#   0x1B - heading MSB
#   0x1C - roll LSB
#   0x1D - roll MSB
#   0x1E - pitch LSB
#   0x1F - pitch MSB
#   data representation (see page 35):
#       1 degree = 16 LSB
#       1 radian = 900 LSB
GYR_DATA_X_LSB      = 0x14  # angular velocities include 14-19
#   data representation - see page 34
#   set UNIT_SEL register to set units (see page 59, 30) - default is dps





class IMU_Driver:
    '''@brief               An IMU driver object
       @details             A class for calibrating and reading from an IMU.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 10, 2021
    '''
    
    def __init__(self, i2c, addr):
        '''@brief           Constructor for IMU driver.
           @details         Sets class variables for the IMU driver.
           @param i2c       I2C object.
           @param addr      Device address.
        '''

        ## I2C object
        self.i2c = i2c
        
        ## Device address
        self.addr = addr

    def set_mode(self, mode):
        '''@brief           Sets the mode for the IMU.
           @details         Writes the passed in mode to the OPR_MODE register.
           @param mode      Mode to set the IMU to.
        '''

        self.i2c.mem_write(mode, self.addr, OPR_MODE)

    def get_calibration_status(self):
        '''@brief           Returns the calibration status.
           @details         Returns a buffer containing the calibration status of each sensor.
           @return          A buffer containing the calibration status of each sensor.
        '''

        buf = bytearray(1)
        self.i2c.mem_read(buf, self.addr, CALIB_STAT)
        return buf

    def get_calibration_coefficients(self):
        '''@brief           Returns the calibration coefficients.
           @details         Reads the calibration coefficients from the registers and returns them.
           @return          Calibration coefficients.
        '''

        buf = bytearray(22)
        self.i2c.mem_read(buf, self.addr, ACC_OFFSET_X_LSB)
        return buf

    def write_calibration_coefficients(self, data):
        '''@brief           Applies the passed in calibration coefficients.
           @details         Writes a buffer of calibration coefficients to the registers.
           @param data      Calibration coefficients to apply.
        '''

        # (see page 48)
        
        # set to config mode
        self.set_mode(CONFIGMODE)
        
        # write the coefficients
        write_addr = ACC_OFFSET_X_LSB
        for i in range(22):
            self.i2c.mem_write(data[i], self.addr, write_addr)
            write_addr += 1

        # set to a fusion mode
        self.set_mode(NDOF)

    def get_euler_angles(self):
        '''@brief       Returns Euler angles.
           @details     Return a tuple containing the angle of the IMU about each axis in radians.
           @return      Euler angles stored in a tuple.
        '''

        # (see page 35)
        
        # read euler angles
        buf = bytearray(6)
        self.i2c.mem_read(buf, self.addr, EUL_HEADING_LSB)

        # unpack and scale
        return self.unpack_and_scale(buf)

    def get_angular_vel(self):
        '''@brief       Returns angular velocities.
           @details     Return a tuple containing the angular velocity of the IMU about each axis in radians/s.
           @return      Angular velocities stored in a tuple.
        '''

        # (see page 34)

        # read angular velocity
        buf = bytearray(6)
        self.i2c.mem_read(buf, self.addr, GYR_DATA_X_LSB)

        # unpack and scale
        return self.unpack_and_scale(buf)

    def unpack_and_scale(self, buf):
        '''@brief      Converts a bytearray to a tuple and scales it.
           @details    Returns a tuple containing a bytearray converted to rad or rad/s.
           @return     Tuple containing the converted values.
        '''
        
        # unpack into 3 signed integers
        buf = struct.unpack('<hhh', buf)

        # scale to convert to degrees or degrees/s
        return tuple(eul_int/16 for eul_int in buf)


if __name__ == '__main__':

    # instantiate IMU driver

    ## I2C object
    i2c = I2C(1, I2C.MASTER)
    
    ## IMU device address
    device_addr = 0x28

    ## IMU
    driver = IMU_Driver(i2c, device_addr)

    # set mode
    driver.set_mode(NDOF)

    # print calibration values until calibrated
    cal_bytes = driver.get_calibration_status()
    while not cal_bytes[0] == 255:
        cal_status = (cal_bytes[0] & 0b11,
                      (cal_bytes[0] & 0b11 << 2) >> 2,
                      (cal_bytes[0] & 0b11 << 4) >> 4,
                      (cal_bytes[0] & 0b11 << 6) >> 6)
        print("Calibration status:", cal_status)
        cal_bytes = driver.get_calibration_status()

    # print Euler angles
    while True:
        angles = driver.get_euler_angles()
        print("Euler angles:", angles)
        utime.sleep_ms(500)
        