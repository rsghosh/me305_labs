'''@file                    Lab_0x01.py
   @brief                   This file contains the code for Lab 0x01.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    September 5, 2021
   
   @package                 Lab_0x01
   @brief                   This file contains the code for Lab 0x01.
   @author                  Ryan Ghosh
   @copyright               This package is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    September 5, 2021
'''

import pyb
import utime
import math

# set up global variables
## Boolean to keep track of whether the button has been pressed since last checked
button_pressed = False

## String to keep track of the current wave type
state = 'start'

# set up button pin and interrupt
## Button pin
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)

def onButtonPressFCN(IRQ_src):
    '''
    @brief          Changes button_pressed to True to indicate that the button has been pressed.
    @details        This function is called on every falling edge of the button pin. It sets
                    button_pressed to True so that update_wave knows to switch wave types.
    @param IRQ_src  Reference to the hardware device that caused the interrupt to occur.
    '''

    # set button_pressed to True
    global button_pressed
    button_pressed = True

## Button interrupt
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

def update_wave(tch):
    '''
    @brief          Updates the LED pwm value.
    @details        This function changes the wave type if the button has been pressed,
                    and resets the reference time. It then updates the pwm percentage for the 
                    LED based on the difference between the current time and the reference time.
    @param tch      The timer channel for the LED.
    '''

    # update state
    global button_pressed, state, ref
    if button_pressed:
        if state == 'square':
            state = 'sine'
            print('sine wave pattern selected')
        elif state == 'sine':
            state = 'sawtooth'
            print('sawtooth wave pattern selected')
        elif state == 'sawtooth' or state == 'start':
            state = 'square'
            print('square wave pattern selected')
        button_pressed = False
        ref = utime.ticks_ms()
    
    # set time difference
    now = utime.ticks_ms()
    time_diff = utime.ticks_diff(now, ref)

    # get pulse_width_percent to update wave
    pwp = 0
    if state == 'square':
        pwp = update_sqw(time_diff)
    elif state == 'sine':
        pwp = update_sw(time_diff)
    elif state == 'sawtooth':
        pwp = update_stw(time_diff)
    
    # set pulse_width_percent
    #print("setting to %d", pwp)
    tch.pulse_width_percent(pwp)

def update_sqw(time_diff):
    '''
    @brief              Returns the pwm percentage for a square wave.
    @details            Given the time difference between now and the reference time,
                        this function returns a value of either 0 or 100 to set the pwm
                        percentage for the LED, following a square wave.
    @param time_diff    The difference in time between now and the reference time.
    @return             Either 0 or 100, depending on whether the LED should be on or off.
    '''

    return 100*(time_diff % 1000 < 500)

def update_sw(time_diff):
    '''
    @brief              Returns the pwm percentage for a sine wave.
    @details            Given the time difference between now and the reference time,
                        this function returns a value from 0 to 100 to set the pwm
                        percentage for the LED, following a sine wave.
    @param time_diff    The difference in time between now and the reference time.
    @return             An integer from 0 to 100 representing the pwm percentage the LED should
                        be set to.
    '''

    return 50 + int(50*math.sin(time_diff*(2*math.pi/10000)))

def update_stw(time_diff):
    '''
    @brief              Returns the pwm percentage for a sawtooth wave.
    @details            Given the time difference between now and the reference time,
                        this function returns a value from 0 to 100 to set the pwm
                        percentage for the LED, following a sawtooth wave.
    @param time_diff    The difference in time between now and the reference time.
    @return             An integer from 0 to 100 representing the pwm percentage the LED should
                        be set to.
    '''

    return 100*(time_diff % 1000 / 1000)

if __name__ == '__main__':
    ## LED pin
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

    ## Timer
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## Timer channel for the LED
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

    # set up reference time
    global ref
    ## Reference time to compare the current time to when setting LED pwm percentage
    ref = utime.ticks_ms()

    # print instructions
    print('Press button B1 to cycle through LED patterns.')

    # loop update_wave
    while(True):
        update_wave(t2ch1)