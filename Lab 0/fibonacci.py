'''@file fibonacci.py
There must be a docstring at the beginning of a Python source file
with an \@file [filename] tag in it!
'''
def fib1 (idx):
    '''
    @brief This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
    Fibonacci number
    '''
    if idx == 0:
        return 0
    elif idx == 1:
        return 1
    else:
        return fib(idx - 1) + fib(idx - 2)
    
def fib (idx):
    '''
    @brief This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
    Fibonacci number
    '''
    # create a list for storing fibonacci results for each index
    # (min length of 2, else enough for every number from 0 to idx)
    lst = [-1] * max(idx + 1, 2)
    
    # fill in numbers at indices 0 and 1
    lst[0] = 0
    lst[1] = 1
    
    # calculate the fibonacci number at idx
    return fib_helper(idx, lst)

def fib_helper(idx, lst):
    '''
    @brief This function is a helper for fib2 that calculates a
    Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
    Fibonacci number
    @param lst A list of previous Fibonacci number results
    '''
    # if the number is already in the list, return it
    if not lst[idx] == -1:
        return lst[idx]
    
    # get fib(idx - 2)
    idx2 = idx - 2
    # (retrieve fib(idx - 2) from the list if it is there)
    if not lst[idx2] == -1:
        f2 = lst[idx2]
    # (else calculate fib(idx - 2))
    else:
        f2 = fib_helper(idx2, lst)
    
    # get fib(idx - 1)
    idx1 = idx - 1
    # (retrieve fib(idx - 1) from the list if it is there)
    if not lst[idx1] == -1:
        f1 = lst[idx1]
    # (else calculate fib(idx - 1))
    else:
        f1 = fib_helper(idx1, lst)
    
    # get the result and store it in the list
    result = f2 + f1
    lst[idx] = result
    
    # return the result
    return result

if __name__ == '__main__':
    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonacci function. Any code
    # within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.
    
    while(True):
        ## The index of the Fibonacci number selected by the user
        idx = input('Enter an index, or q to exit: ')
        
        # get valid input
        while not (idx.isdigit() or idx == 'q'):
            idx = input('Invalid input. Enter a natural number, or q to exit: ')
        
        # exit if q is entered
        if idx == 'q':
            break
        
        # print the fibonacci number at the corresponding index
        print ('Fibonacci number at '
               'index {:} is {:}.'.format(idx,fib(int(idx))))