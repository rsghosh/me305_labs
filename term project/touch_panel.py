'''@file                    touch_panel.py
   @brief                   This file contains the touch panel driver for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

import pyb
import utime
from ulab import numpy as np

class Touch_Panel:
    '''@brief               A touch panel driver object
       @details             A class for calibrating and reading from a touch panel.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 9, 2021
    '''

    def __init__(self, xp, xm, yp, ym, Kxx, Kxy, Kyx, Kyy, Xc, Yc):
        '''@brief           Constructor for touch panel driver.
           @details         Sets class variables for the touch panel driver.
           @param xp        xp pin.
           @param xm        xm pin.
           @param yp        yp pin.
           @param ym        ym pin.
           @param Kxx       Kxx coefficient.
           @param Kxy       Kxy coefficient.
           @param Kyx       Kyx coefficient.
           @param Kyy       Kyy coefficient.
           @param Xc        Xc coefficient.
           @param Yc        Yc coefficient.
        '''

        # pin names

        ## xp pin name
        self.xp_name = xp.name()
        
        ## xm pin name
        self.xm_name = xm.name()
        
        ## yp pin name
        self.yp_name = yp.name()
        
        ## ym pin name
        self.ym_name = ym.name()

        # calibration coefficients

        ## Kxx coefficient
        self.Kxx = Kxx
        
        ## Kxy coefficient
        self.Kxy = Kxy
        
        ## Kyx coefficient
        self.Kyx = Kyx
        
        ## Kyy coefficient
        self.Kyy = Kyy
        
        ## Xc coefficient
        self.Xc = Xc
        
        ## Yc coefficient
        self.Yc = Yc

        #other

        ## Distance between measurement points, in mm
        self.point_dist_mm = 40
        
        ## Settle time
        self.settle_time = 4
        
        ## Z threshold for detecting a touch
        self.z_threshold = 1000

    def set_coefficients(self, Kxx, Kxy, Kyx, Kyy, Xc, Yc):
        '''@brief           Set coefficients.
           @details         Set coefficients to what is passed in.
           @param Kxx       Kxx coefficient.
           @param Kxy       Kxy coefficient.
           @param Kyx       Kyx coefficient.
           @param Kyy       Kyy coefficient.
           @param Xc        Xc coefficient.
           @param Yc        Yc coefficient.
        '''

        self.Kxx = Kxx
        self.Kxy = Kxy
        self.Kyx = Kyx
        self.Kyy = Kyy
        self.Xc = Xc
        self.Yc = Yc

    def calibrate(self):
        '''@brief       Calibrates the touch panel.
           @details     Prompts the user to touch points on the panel to calibrate it.
           @return      Calibration coefficients.
        '''

        # array of raw readings
        X = np.array([[0, 0, 1],
                      [0, 0, 1],
                      [0, 0, 1],
                      [0, 0, 1],
                      [0, 0, 1],
                      [0, 0, 1],
                      [0, 0, 1],
                      [0, 0, 1],
                      [0, 0, 1]])

        # corresponding point locations
        X_actual = np.array([[-1, 1],
                             [0, 1],
                             [1, 1],
                             [1, 0],
                             [0, 0],
                             [-1, 0],
                             [-1, -1],
                             [0, -1],
                             [1, -1]])

        # point ordering:
        # 0 -> 1 -> 2
        #           |
        # 5 <- 4 <- 3
        # |
        # 6 -> 7 -> 8
        
        # convert corresponding point locations to m
        X_actual *= self.point_dist_mm / 1000
        
        # collect raw readings
        for i in range(9):
            input("Touch and hold on point {}, then press enter.".format(i))
            X[i,0] = self.scan_x_raw()
            X[i,1] = self.scan_y_raw()
        
        # get coefficients
        #B = np.matmul(np.matmul(np.linalg.inv(np.matmul(np.transpose(X), X)), np.transpose(X)), X_actual)
        B = np.dot(np.dot(np.linalg.inv(np.dot(X.transpose(), X)), X.transpose()), X_actual)
        self.Kxx = B[0, 0]
        self.Kyx = B[0, 1]
        self.Kxy = B[1, 0]
        self.Kyy = B[1, 1]
        self.Xc = B[2, 0]
        self.Yc = B[2, 1]

        return self.Kxx, self.Kxy, self.Kyx, self.Kyy, self.Xc, self.Yc

    def scan_x(self):
        '''@brief           Return x scan value.
           @details         Return the x-location of the touch in m.
           @return          x-location of the touch.
        '''

        return self.Kxx * self.scan_x_raw() + self.Kxy * self.scan_y_raw() + self.Xc

    def scan_y(self):
        '''@brief           Return y scan value.
           @details         Return the y-location of the touch in m.
           @return          y-location of the touch.
        '''

        return self.Kyx * self.scan_x_raw() + self.Kyy * self.scan_y_raw() + self.Yc

    def scan_z(self):
        '''@brief           Return z scan value.
           @details         Return whether a touch is detected.
           @return          Boolean value of whether a touch is detected.
        '''
        self.ym = pyb.Pin(self.ym_name, pyb.Pin.IN)
        self.xm = pyb.Pin(self.xm_name, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_name, pyb.Pin.OUT_PP)
        self.xp = pyb.Pin(self.xp_name, pyb.Pin.IN)
        adc = pyb.ADC(self.ym)
        self.yp.value(1)
        self.xm.value(0)
        utime.sleep_us(self.settle_time)
        return adc.read() < self.z_threshold

    def scan_all(self):
        '''@brief       Return x, y, and z scan values.
           @details     Returns the x and y-locations of the touch in m, and whether a touch was detected.
           @return      tuple containing the x, y, and z converted scan values.
        '''

        adc_x, adc_y, z = self.scan_all_raw()
        
        x = self.Kxx * adc_x + self.Kxy * adc_y + self.Xc
        y = self.Kyx * adc_x + self.Kyy * adc_y + self.Yc

        return x,y,z

    def scan_x_raw(self):
        '''@brief           Return x scan value.
           @details         Return the raw reading of the x-location of the touch.
           @return          raw x-location of the touch.
        '''

        self.ym = pyb.Pin(self.ym_name, pyb.Pin.IN)
        self.xm = pyb.Pin(self.xm_name, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_name, pyb.Pin.IN)
        self.xp = pyb.Pin(self.xp_name, pyb.Pin.OUT_PP)
        adc = pyb.ADC(self.ym)
        self.xp.value(1)
        self.xm.value(0)
        utime.sleep_us(self.settle_time)
        return adc.read()
    
    def scan_y_raw(self):
        '''@brief           Return y scan value.
           @details         Return the raw reading of the y-location of the touch.
           @return          raw y-location of the touch.
        '''

        self.ym = pyb.Pin(self.ym_name, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(self.xm_name, pyb.Pin.IN)
        self.yp = pyb.Pin(self.yp_name, pyb.Pin.OUT_PP)
        self.xp = pyb.Pin(self.xp_name, pyb.Pin.IN)
        adc = pyb.ADC(self.xm)
        self.yp.value(1)
        self.ym.value(0)
        utime.sleep_us(self.settle_time)
        return adc.read()

    def scan_all_raw(self):
        '''@brief       Return x, y, and z scan values.
           @details     Returns the raw x and y-locations of the touch, and whether a touch was detected.
           @return      tuple containing the x, y, and z scan values.
        '''

        # scan x
        self.ym = pyb.Pin(self.ym_name, pyb.Pin.IN)
        self.xm = pyb.Pin(self.xm_name, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_name, pyb.Pin.IN)
        self.xp = pyb.Pin(self.xp_name, pyb.Pin.OUT_PP)
        adc = pyb.ADC(self.ym)
        self.xp.value(1)
        self.xm.value(0)
        utime.sleep_us(self.settle_time)
        x = adc.read()

        # scan z
        #self.ym = pyb.Pin(self.ym_name, pyb.Pin.IN)
        #self.xm = pyb.Pin(self.xm_name, pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_name, pyb.Pin.OUT_PP)
        self.xp = pyb.Pin(self.xp_name, pyb.Pin.IN)
        #adc = pyb.ADC(self.ym)
        self.yp.value(1)
        #self.xm.value(0)
        utime.sleep_us(self.settle_time)
        z = adc.read() < self.z_threshold

        # scan y
        self.ym = pyb.Pin(self.ym_name, pyb.Pin.OUT_PP)
        self.xm = pyb.Pin(self.xm_name, pyb.Pin.IN)
        #self.yp = pyb.Pin(self.yp_name, pyb.Pin.OUT_PP)
        #self.xp = pyb.Pin(self.xp_name, pyb.Pin.IN)
        adc = pyb.ADC(self.xm)
        #self.yp.value(1)
        self.ym.value(0)
        utime.sleep_us(self.settle_time)
        y = adc.read()

        return x,y,z
