'''@file                    task_collection.py
   @brief                   This file contains the data collection task for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

import utime
import os

S0_INIT         = 0
S1_BALANCE      = 1
S2_COLLECT      = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4

class Task_Collection:
    '''@brief               A task collection object.
       @details             A class for collecting data on the position and velocity of the ball and table.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 9, 2021
    '''

    def __init__(self, filename, buffer_size, period, shared_vars):
        '''@brief               Constructor for data collection task.
           @details             Sets class variables for the data collection task.
           @param filename      Name of the file to write to.
           @param buffer_size   Size of the buffer used when writing to the file.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Name of the file
        self.filename = filename
        
        ## Buffer size for writing to the file
        self.buffer_size = buffer_size

        ## File
        self.f = open(filename, 'w+', buffering=buffer_size)

        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the encoder and user tasks
        self.shared_vars = shared_vars
        
        self.update_next_time()

    def run(self):
        '''@brief           Runs the data collection task.
           @details         Writes data to a file if requested.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            # write data to file
            state = self.shared_vars['state']
            if state == S2_COLLECT:
                time = utime.ticks_diff(utime.ticks_ms(), self.shared_vars['ref time'])
                angles = self.shared_vars['imu angles']
                vels = self.shared_vars['imu vels']
                pwm_x = self.shared_vars['pwm_x']
                pwm_y = self.shared_vars['pwm_y']
                self.f.write(f"{time},{angles[0]},{angles[1]},{vels[0]},{vels[1]},{pwm_x},{pwm_y}\n")
            elif self.shared_vars['close file']:
                self.f.close()
                self.shared_vars['close file'] = False

    def update_next_time(self):
        '''@brief           Returns the next time the data collection task should run.
           @details         Adds period to the current time to calculate the next time the data collection task should run.
           @return          The next time the data collection task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        