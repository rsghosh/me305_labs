'''@file                    task_imu.py
   @brief                   This file contains the IMU task for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

import imu_driver
import utime
import os
import struct

S0_INIT         = 0
S1_BALANCE      = 1
S2_COLLECT      = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4

# https://cdn-shop.adafruit.com/datasheets/BST_BNO055_DS000_12.pdf

# calibration procedure - see page 47

# modes (see page 20):
CONFIGMODE      = 0
ACCONLY         = 1
MAGONLY         = 2
GYROONLY        = 3
ACCMAG          = 4
ACCGYRO         = 5
MAGGYRO         = 6
ACCMAG          = 7
IMU             = 8
COMPASS         = 9
M4G             = 10
NDOF_FMC_OFF    = 11
NDOF            = 12

class Task_IMU:
    '''@brief               An IMU task object
       @details             A class for calibrating and reading from an IMU.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 9, 2021
    '''

    def __init__(self, imu_driver, period, shared_vars):
        '''@brief               Constructor for motor task.
           @details             Sets class variables for the motor task.
           @param imu_driver    IMU_Driver object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## IMU driver
        self.imu = imu_driver
        self.imu.set_mode(NDOF)

        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the encoder and user tasks
        self.shared_vars = shared_vars
        
        self.calibrate()
        self.update_next_time()

    def calibrate(self):
        '''@brief           Makes sure the IMU is calibrated.
           @details         Either reads calibration coefficients from a file if there is one, or calibrates the IMU and writes the new coefficients
        '''

        filename = "IMU_cal_coeffs.txt"

        # if file exists, read calibration data from it
        if filename in os.listdir():
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [int(cal_value, 16) for cal_value in cal_data_string.strip().split(',')]
                self.imu.write_calibration_coefficients(cal_values)
        
        # else calibrate the IMU and write the coefficients to the file
        else:
            with open(filename, 'w') as f:
                
                # print calibration values until calibrated
                cal_bytes = self.imu.get_calibration_status()
                while not cal_bytes[0] == 255:
                    cal_status = (cal_bytes[0] & 0b11,
                                (cal_bytes[0] & 0b11 << 2) >> 2,
                                (cal_bytes[0] & 0b11 << 4) >> 4,
                                (cal_bytes[0] & 0b11 << 6) >> 6)
                    print("Calibration status:", cal_status)
                    cal_bytes = self.imu.get_calibration_status()
                
                # get calibration data
                cal_values = self.imu.get_calibration_coefficients()
                
                # convert to array of ints
                cal_values = struct.unpack('<bbbbbbbbbbbbbbbbbbbbbb', cal_values)
                
                # convert to array of strings
                cal_values_str = [hex(cal_value) for cal_value in cal_values]
                
                # write calibration data
                f.write(','.join(cal_values_str) + "\r\n")

    def run(self):
        '''@brief           Runs the IMU task.
           @details         Controls the IMU driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            # update imu data in shared variables
            state = self.shared_vars['state']
            if state == S1_BALANCE or state == S2_COLLECT:
                self.shared_vars['imu angles'] = self.imu.get_euler_angles()
                self.shared_vars['imu vels'] = self.imu.get_angular_vel()

    def update_next_time(self):
        '''@brief           Returns the next time the IMU task should run.
           @details         Adds period to the current time to calculate the next time the IMU task should run.
           @return          The next time the IMU task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
