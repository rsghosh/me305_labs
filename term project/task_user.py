'''@file                    task_user.py
   @brief                   This file contains the user task for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

import pyb
import utime
import math

S0_INIT         = 0
S1_BALANCE      = 1
S2_COLLECT      = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4

class Task_User:
    '''@brief               A user task object
       @details             A class for taking in user input and responding to it.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 9, 2021
    '''

    def __init__(self, period, shared_vars):
        '''@brief               Constructor for user task.
           @details             Sets class variables for the user task object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Serial port
        self.ser_port = pyb.USB_VCP()
        
        ## Current state
        self.state = S0_INIT
        
        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the tasks
        self.shared_vars = shared_vars

        self.update_next_time()
        self.reset_state()

        ## Whether data collection is in progress
        self.started_collecting = False

    def reset_state(self):
        '''@brief           Resets the state of the user task.
           @details         Changes state to 0 and prints options for the user.
        '''

        self.shared_vars['state'] = S0_INIT
        print('''Enter a commmand:\n
            1 - Begin balancing\n
            2 - Start data collection\n
            c or C - Clear a fault condition triggered by the DRV8847\n
            s or S - End data collection\n\n''')

    def run(self):
        '''@brief           Runs the user task.
           @details         Sets motor speeds and collects data from encoders based on user input.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()
            
            # continue current state
            if not (self.shared_vars['request clear fault'] 
                    or self.shared_vars['request motor1 stop']
                    or self.shared_vars['request motor2 stop']):
                if (self.shared_vars['state'] == S3_CLEAR_FAULT):
                    self.reset_state()
                elif (self.shared_vars['state'] == S1_BALANCE
                    or self.shared_vars['state'] == S2_COLLECT):
                    # transition to end state if there is a fault
                    # if self.shared_vars['fault detected']:
                    #     print("Motor fault occurred.")
                    #     self.shared_vars['state'] = S4_END
                    if self.shared_vars['state'] == S2_COLLECT:
                        if not self.started_collecting:
                            now = utime.ticks_ms()
                            self.shared_vars['ref time'] = now
                            self.started_collecting = True
                elif self.shared_vars['state'] == S4_END:
                    self.shared_vars['close file'] = True
                    self.started_collecting = False
                    self.reset_state()

            # update state
            if self.ser_port.any():
                user_in = self.ser_port.read(1)

                if user_in == b'1':
                    self.shared_vars['state'] = S1_BALANCE
                elif user_in == b'2':
                    self.shared_vars['state'] = S2_COLLECT
                elif (user_in == b'c' or user_in == b'C'):
                    self.shared_vars['state'] = S3_CLEAR_FAULT 
                elif (user_in == b's' or user_in == b'S'):
                    # stop the motor
                    if self.shared_vars['state'] == S1_BALANCE or self.shared_vars['state'] == S2_COLLECT:
                        self.shared_vars['request motor1 stop'] = True
                        self.shared_vars['request motor2 stop'] = True
                    # transition to end state
                    self.shared_vars['state'] = S4_END


    def update_next_time(self):
        '''@brief           Returns the next time the user task should run.
           @details         Adds period to the current time to calculate the next time the user task should run.
           @return          The next time the user task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
