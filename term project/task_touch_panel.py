'''@file                    task_touch_panel.py
   @brief                   This file contains the touch panel task for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

import touch_panel
import utime
import os

S0_INIT         = 0
S1_BALANCE      = 1
S2_COLLECT      = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4

class Task_Touch_Panel:
    '''@brief               An IMU task object
       @details             A class for calibrating and reading from a touch panel.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 9, 2021
    '''

    def __init__(self, touch_panel, period, shared_vars):
        '''@brief               Constructor for motor task.
           @details             Sets class variables for the motor task.
           @param touch_panel   Touch_Panel object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Touch panel driver
        self.touch_panel = touch_panel

        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the encoder and user tasks
        self.shared_vars = shared_vars

        ## Previous x value
        self.prev_x = None

        ## Previous y value
        self.prev_y = None

        self.calibrate()        
        self.update_next_time()

    def calibrate(self):
        '''@brief           Makes sure the touch panel is calibrated.
           @details         Either reads calibration coefficients from a file if there is one, or calibrates the touch panel and writes the new coefficients
        '''

        filename = "RT_cal_coeffs.txt"
        
        # if file exists, read calibration coefficients from it
        if filename in os.listdir():
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                self.touch_panel.set_coefficients(cal_values[0], cal_values[1], cal_values[2], cal_values[3], cal_values[4], cal_values[5])
        
        # else calibrate the touch panel and write the coefficients to the file
        else:
            with open(filename, 'w') as f:
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = self.touch_panel.calibrate()
                f.write(f"{Kxx},{Kxy},{Kyx},{Kyy},{Xc},{Yc}\r\n")

    def run(self):
        '''@brief           Runs the touch panel task.
           @details         Controls the touch panel driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            # update touch panel data in shared variables
            state = self.shared_vars['state']
            if state == S1_BALANCE or state == S2_COLLECT:
                tp_scan = self.touch_panel.scan_all()
                if self.prev_x is None:
                    self.prev_x = tp_scan[0]
                if self.prev_y is None:
                    self.prev_y = tp_scan[1]
                x_dot = (tp_scan[0] - self.prev_x) / (self.period/1000)
                y_dot = (tp_scan[1] - self.prev_y) / (self.period/1000)
                self.prev_x = tp_scan[0]
                self.prev_y = tp_scan[1]

                self.shared_vars['tp data'] = tp_scan
                self.shared_vars['x_dot'] = x_dot
                self.shared_vars['y_dot'] = y_dot

    def update_next_time(self):
        '''@brief           Returns the next time the touch panel task should run.
           @details         Adds period to the current time to calculate the next time the touch panel task should run.
           @return          The next time the touch panel task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
