'''@file                    task_motor.py
   @brief                   This file contains the motor task for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

import DRV8847
import closedloop
import utime
from ulab import numpy as np

S0_INIT         = 0
S1_BALANCE      = 1
S2_COLLECT      = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4

class Task_Motor:
    '''@brief               A motor task object
       @details             A class for controlling 2 motors.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 9, 2021
    '''

    def __init__(self, motor_driver, motor1, motor2, K1, K2, scalar, period, shared_vars):
        '''@brief               Constructor for motor task.
           @details             Sets class variables for the motor task.
           @param motor_driver  DRV8847 object.
           @param motor1        1st Motor object.
           @param motor2        2nd Motor object.
           @param K1            Array of gains for motor1.
           @param K2            Array of gains for motor2.
           @param scalar        Factor to scale the closedloop output by to convert to pwm.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Motor driver
        self.motor_driver = motor_driver
        
        ## Motor 1
        self.motor1 = motor1
        
        ## Motor 2
        self.motor2 = motor2
        
        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the tasks
        self.shared_vars = shared_vars

        ## Closed loop controller for motor1
        self.cl1 = closedloop.ClosedLoop(K1, scalar, 100)

        ## Closed loop controller for motor2
        self.cl2 = closedloop.ClosedLoop(K2, scalar, 100)

        self.motor_driver.enable()
        self.update_next_time()
    
    def run(self):
        '''@brief           Runs the motor task.
           @details         Controls the motor driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            if self.shared_vars['state'] == S1_BALANCE or self.shared_vars['state'] == S2_COLLECT:
                # Motor1
                
                # # check for fault
                # if self.motor_driver.fault_triggered:
                #     self.shared_vars['fault detected'] = True
                #     self.motor1.set_duty(0)
                # # else set motor 1 duty cycle
                # else:

                # get state: transpose of [x, theta_y, x_dot, theta_dot_y]
                x = self.shared_vars['tp data'][0]
                theta_y = self.shared_vars['imu angles'][1]
                x_dot = self.shared_vars['x_dot']
                theta_dot_y = self.shared_vars['imu vels'][1]
                #x_state = np.transpose(np.atleast_2d(np.array([x, theta_y, x_dot, theta_dot_y])))
                x_state = np.array([x, theta_y, x_dot, theta_dot_y]).transpose()

                # set duty cycle
                pwm = self.cl1.update(x_state)
                self.motor1.set_duty(pwm)
                self.shared_vars['pwm_x'] = pwm


                # Motor 2

                # # check for fault
                # if self.motor_driver.fault_triggered:
                #     self.shared_vars['fault detected'] = True
                #     self.motor2.set_duty(0)
                # # else set motor 2 duty cycle
                # else:

                # get state: transpose of [y, theta_x, y_dot, theta_dot_x]
                y = self.shared_vars['tp data'][1]
                theta_x = self.shared_vars['imu angles'][0]
                y_dot = self.shared_vars['y_dot']
                theta_dot_x = self.shared_vars['imu vels'][0]
                #y_state = np.transpose(np.atleast_2d(np.array([y, theta_x, y_dot, theta_dot_x])))
                y_state = np.array([y, theta_x, y_dot, theta_dot_x]).transpose()

                # set duty cycle
                pwm = self.cl2.update(y_state)
                self.motor2.set_duty(pwm)
                self.shared_vars['pwm_y'] = pwm
                
            elif self.shared_vars['state'] == S4_END:
                # stop motor
                if self.shared_vars['request motor1 stop']:
                    self.motor1.set_duty(0)
                    self.shared_vars['request motor1 stop'] = False
                if self.shared_vars['request motor2 stop']:
                    self.motor2.set_duty(0)
                    self.shared_vars['request motor2 stop'] = False
            elif self.shared_vars['state'] == S3_CLEAR_FAULT:
                self.motor_driver.enable()
                self.shared_vars['request clear fault'] = False
                self.shared_vars['fault detected'] = False

    def update_next_time(self):
        '''@brief           Returns the next time the motor task should run.
           @details         Adds period to the current time to calculate the next time the motor task should run.
           @return          The next time the motor task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
