'''@file                    closedloop.py
   @brief                   This file contains the ClosedLoop class for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

from ulab import numpy as np

class ClosedLoop:
    '''@brief               A ClosedLoop object
       @details             A P controller class for generating an actuation value based on measured and reference values and a gain. 
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                December 9, 2021
    '''

    def __init__(self, K, scalar, max_pwm):
        '''@brief               Constructor for closedloop controller.
           @details             Sets class variables for the closedloop object.
           @param K             Array of gains.
           @param scalar        Factor to scale the output by to convert to pwm.
           @param max_pwm       Maximum actuation value.
        '''

        ## Array of gains
        self.K = K

        ## Factor to scale the output by to convert to pwm
        self.scalar = scalar

        ## Maximum actuation value
        self.max_pwm = max_pwm

    def update(self, x):
        '''@brief           Gives an actuation value.
           @details         Returns an actuation value baed on measured value, reference value, proportional gain, and max value.
           @param x         Column matrix of state variables.
           @return          Actuation value.
        '''
        #pwm = np.atleast_2d(-np.matmul(self.K, x)) * self.scalar
        pwm = -np.dot(self.K, x) * self.scalar

        if pwm < 0:
            return max(pwm, -self.max_pwm)
        return min(pwm, self.max_pwm)
