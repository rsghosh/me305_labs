'''@file                    main.py
   @brief                   This file contains the main function for Lab 0xFF.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    December 9, 2021
'''

import task_collection
import task_imu
import task_motor
import task_touch_panel
import task_user
import drv8847
import imu_driver
import touch_panel
import pyb
from pyb import I2C
from ulab import numpy as np

S0_INIT         = 0
S1_COLLECT1     = 1
S2_COLLECT2     = 2
S3_CLEAR_FAULT  = 3
S4_END          = 4

if __name__ == '__main__':
    # IMU setup
    i2c = I2C(1, I2C.MASTER)
    device_addr = 0x28

    ## IMU
    imu = imu_driver.IMU_Driver(i2c, device_addr)


    # motor setup

    ## nSleep pin for motor driver
    nSleep = pyb.Pin(pyb.Pin.board.PA15, pyb.Pin.OUT_PP)

    ## nFault pin for motor driver
    nFault = pyb.Pin(pyb.Pin.board.PB2, pyb.Pin.IN)

    ## Input1 for motor driver
    in1 = pyb.Pin(pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
    
    ## Input2 for motor driver
    in2 = pyb.Pin(pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
    
    ## Input3 for motor driver
    in3 = pyb.Pin(pyb.Pin.board.PB0, pyb.Pin.OUT_PP)
    
    ## Input4 for motor driver
    in4 = pyb.Pin(pyb.Pin.board.PB1, pyb.Pin.OUT_PP)

    ## Timer for motor driver
    tim3 = pyb.Timer(3, freq=20000)

    ## Timer channel1 for motor driver
    t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=in1)
    
    ## Timer channel2 for motor driver
    t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=in2)
    
    ## Timer channel3 for motor driver
    t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin=in3)
    
    ## Timer channel4 for motor driver
    t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin=in4)

    # Create a motor driver object and two motor objects
    
    ## Motor driver
    motor_drv = drv8847.DRV8847(nSleep, nFault)
    
    ## Motor 1
    motor_1 = motor_drv.motor(in1, in2, t3ch1, t3ch2)
    
    ## Motor 2
    motor_2 = motor_drv.motor(in3, in4, t3ch3, t3ch4)


    # touch panel setup

    ## ym pin for touch panel
    ym = pyb.Pin(pyb.Pin.board.PA0, pyb.Pin.IN)
    
    ## xm pin for touch panel
    xm = pyb.Pin(pyb.Pin.board.PA1, pyb.Pin.OUT_PP)
    
    ## yp pin for touch panel
    yp = pyb.Pin(pyb.Pin.board.PA6, pyb.Pin.IN)
    
    ## xp pin for touch panel
    xp = pyb.Pin(pyb.Pin.board.PA7, pyb.Pin.OUT_PP)

    ## Touch panel
    tp = touch_panel.Touch_Panel(xp, xm, yp, ym, 0, 0, 0, 0, 0, 0)


    ## Shared variables
    shared_vars = {
        'state':                S0_INIT,
        'ref time':             0,
        'tp data':              (0, 0, 0),
        'x_dot':                0,
        'y_dot':                0,
        'imu angles':           (0, 0, 0),
        'imu vels':             (0, 0, 0),
        'pwm_x':                0,
        'pwm_y':                0,
        'request clear fault':  False,
        'request motor1 stop':  False,
        'request motor2 stop':  False,
        'fault detected':       False,
        'close file':           False
    }


    ## Task Collection
    t_collection = task_collection.Task_Collection('data.txt', 10, 1, shared_vars)

    ## Task IMU
    t_imu = task_imu.Task_IMU(imu, 15, shared_vars)

    ## Motor1 gains
    K1 = np.array([-0.166, -0.121, -0.0085, -0.00079])
    
    ## Motor1 gains
    K2 = np.array([-0.166, -0.121, -0.0085, -0.00079])
    
    ## Task Motor
    t_motor = task_motor.Task_Motor(motor_drv, motor_1, motor_2, K1, K2, 333.64, 1, shared_vars)

    ## Task Touch Panel
    t_touch_panel = task_touch_panel.Task_Touch_Panel(tp, 1, shared_vars)

    ## Task User Interface
    t_user = task_user.Task_User(1, shared_vars)

    # loop tasks
    while(True):
        t_imu.run()
        t_touch_panel.run()
        t_motor.run()
        t_collection.run()
        t_user.run()
