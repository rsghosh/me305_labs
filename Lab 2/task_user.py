'''@file                    task_user.py
   @brief                   This file contains the user task for Lab 0x02.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    September 19, 2021
'''

import pyb
import utime

S0_INIT     = 0
S1_ZERO     = 1
S2_PRINT    = 2
S3_DELTA    = 3
S4_COLLECT  = 4
S5_END      = 5

class Task_User:
    '''@brief               A user task object
       @details             A class for taking in user input and responding to it.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                September 19, 2021
    '''

    def __init__(self, period, shared_vars):
        '''@brief               Constructor for user task.
           @details             Sets class variables for the user task object.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## Serial port
        self.ser_port = pyb.USB_VCP()
        
        ## Current state
        self.state = S0_INIT
        
        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of varialbes used for communication between the encoder and user tasks
        self.shared_vars = shared_vars

        self.update_next_time()
        self.reset_state()

        ## Whether data collection is in progress
        self.started_collecting = False

        ## List for storing timestamps for collected data
        self.time_lst = []

        ## List for storing positions for collected data
        self.pos_lst = []

        ## Time increment in milliseconds for collecting data
        self.t_increment = 50

        ## Time to collect next sample
        self.next_sample = 0

    def reset_state(self):
        '''@brief           Resets the state of the user task.
           @details         Changes state to 0 and prints options for the user.
        '''

        self.shared_vars['state'] = S0_INIT
        print('''Enter a commmand:\n
            z - Zero the position of encoder 1\n
            p - Print out the position of encoder 1\n
            d - Print out the delta for encoder 1\n
            g - Collect encoder 1 data for 30 seconds
            and print it to PuTTY as a comma separated list\n
            s - End data collection prematurely\n''')

    def run(self):
        '''@brief           Runs the user task.
           @details         Requests or collects data from an encoder task and updates the state based on user input.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()
            
            # continue current state
            if self.shared_vars['unread result']:
                if self.shared_vars['state'] == S1_ZERO:
                    self.reset_state()
                elif (self.shared_vars['state'] == S2_PRINT
                    or self.shared_vars['state'] == S3_DELTA):
                    print(self.shared_vars['result'])
                    self.reset_state()
                elif self.shared_vars['state'] == S4_COLLECT:
                    # get current time
                    now = utime.ticks_ms()
                    # if collection has not started already, set the reference time
                    # and the next time to collect data
                    if not self.started_collecting:
                        self.shared_vars['ref time'] = now
                        self.next_sample = now
                        self.started_collecting = True
                    # collect data if time passed is less than 30 seconds
                    # and t_increment has passed since the last data collection
                    if utime.ticks_diff(now, self.shared_vars['ref time']) <= 30000:
                        if utime.ticks_diff(now, self.next_sample) >= 0:
                            # set next_time
                            self.next_sample = utime.ticks_add(now, self.t_increment)
                            # add current time relative to ref
                            self.time_lst.append(utime.ticks_diff(now, self.shared_vars['ref time']))
                            # add current encoder1 position
                            self.pos_lst.append(self.shared_vars['result'])
                    # if 30 seconds has passed, print the readings
                    else:
                        self.print_readings(self.time_lst, self.pos_lst)
                        # clear the readings and reset state
                        self.time_lst = []
                        self.pos_lst = []
                        self.started_collecting = False
                        self.reset_state()
                elif self.shared_vars['state'] == S5_END:
                    # print the readings
                    self.print_readings(self.time_lst, self.pos_lst)
                    # clear the readings and reset state
                    self.time_lst = []
                    self.pos_lst = []
                    self.started_collecting = False
                    self.reset_state()
                self.shared_vars['unread result'] = False

            # update state
            if self.ser_port.any():
                user_in = self.ser_port.read(1)

                if user_in == b'z':
                    self.shared_vars['state'] = S1_ZERO
                elif user_in == b'p':
                    self.shared_vars['state'] = S2_PRINT
                elif user_in == b'd':
                    self.shared_vars['state'] = S3_DELTA
                elif user_in == b'g':
                    self.shared_vars['state'] = S4_COLLECT
                elif user_in == b's':
                    self.shared_vars['state'] = S5_END

    def update_next_time(self):
        '''@brief           Returns the next time the user task should run.
           @details         Adds period to the current time to calculate the next time the user task should run.
           @return          The next time the user task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

    def print_readings(self, time_lst, pos_lst):
        '''@brief           Prints a list of encoder readings with timestamps.
           @details         Prints encoder readings in "<timestamp>, <position>" format, with one reading per line.
        '''

        print('time, pos:')
        for i in range(len(time_lst)):
            print('{}, {}'.format(time_lst[i], pos_lst[i]))
