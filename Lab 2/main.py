'''@file                    main.py
   @brief                   This file contains the main function for Lab 0x02.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    September 19, 2021
'''

import encoder
import task_encoder
import task_user
import pyb

S0_INIT     = 0
S1_ZERO     = 1
S2_PRINT    = 2
S3_DELTA    = 3
S4_COLLECT  = 4
S5_END      = 5

if __name__ == '__main__':
    
    ## Pin A of encoder1
    pinA = pyb.Pin(pyb.Pin.board.PB6, pyb.Pin.IN)

    ## Pin B of encoder1
    pinB= pyb.Pin(pyb.Pin.board.PB7, pyb.Pin.IN)

    ## Timer for encoder1
    tim4 = pyb.Timer(4, prescaler=0, period=65535)

    ## timer channel1 for encoder 1
    t4ch1 = tim4.channel(1, pyb.Timer.ENC_A, pin=pinA)

    ## timer channel2 for encoder1
    t4ch2 = tim4.channel(2, pyb.Timer.ENC_B, pin=pinB)

    ## Encoder1
    enc1 = encoder.Encoder(pinA, pinB, tim4)

    ## Shared variables for the encoder and user tasks
    shared_vars = {
        'state':        S0_INIT,
        'ref time':     0,
        'result':       0,
        'unread result':  False
    }

    ## Task Encoder
    t_enc = task_encoder.Task_Encoder(enc1, 1, shared_vars)

    ## Task User Interface
    t_user = task_user.Task_User(2, shared_vars)

    # loop tasks
    while(True):
        t_enc.run()
        t_user.run()
