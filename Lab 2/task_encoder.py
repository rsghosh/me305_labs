'''@file                    task_encoder.py
   @brief                   This file contains the encoder task for Lab 0x02.
   @author                  Ryan Ghosh
   @copyright               This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
   @date                    September 19, 2021
'''

import encoder
import utime

S0_INIT     = 0
S1_ZERO     = 1
S2_PRINT    = 2
S3_DELTA    = 3
S4_COLLECT  = 4
S5_END      = 5

class Task_Encoder:
    '''@brief               An encoder task object
       @details             A class for getting data from an encoder driver.
       @author              Ryan Ghosh
       @copyright           This file is licensed under the CC BY-NC-SA 4.0
                            Please visit https://creativecommons.org/licenses/by-nc-sa/4.0/
                            for terms and conditions of the license.
       @date                September 19, 2021
    '''

    def __init__(self, encoder, period, shared_vars):
        '''@brief               Constructor for encoder task.
           @details             Sets class variables for the encoder task object.
           @param encoder       The encoder object to control.
           @param period        Period of time to wait between running the task.
           @param shared_vars   Dictionary of variables used for communication between the encoder and user tasks.
        '''

        ## The encoder object to control
        self.enc = encoder

        ## Period of time to wait between running the task
        self.period = period

        ## Dictionary of variables used for communication between the encoder and user tasks
        self.shared_vars = shared_vars
        
        self.update_next_time()

    def run(self):
        '''@brief           Runs the encoder task.
           @details         Controls the encoder driver based on requests from a user task.
        '''

        if utime.ticks_diff(utime.ticks_us(), self.next_time) >= 0:
            self.update_next_time()

            # update
            self.enc.update()
            
            # collect requested value
            if self.shared_vars['unread result'] == False:
                if self.shared_vars['state'] == S1_ZERO:
                    self.enc.set_position(0)
                elif self.shared_vars['state'] == S2_PRINT:
                    self.shared_vars['result'] = self.enc.get_position()
                elif self.shared_vars['state'] == S3_DELTA:
                    self.shared_vars['result'] = self.enc.get_delta()
                elif self.shared_vars['state'] == S4_COLLECT:
                    self.shared_vars['result'] = self.enc.get_position()
                self.shared_vars['unread result'] = True

    def update_next_time(self):
        '''@brief           Returns the next time the encoder task should run.
           @details         Adds period to the current time to calculate the next time the encoder task should run.
           @return          The next time the encoder task should run.
        '''

        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
